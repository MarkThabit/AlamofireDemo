//
//  AppDelegate.swift
//  AlamofireDemo
//
//  Created by Mark Maged on 2/17/18.
//  Copyright © 2018 Mark Maged. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions
        launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        return true
    }
}

