//
//  Artist.swift
//  AlamofireDemo
//
//  Created by Mark Maged on 2/17/18.
//  Copyright © 2018 Mark Maged. All rights reserved.
//

import Foundation

class Artist
{
    var id: Int64?
    var name: String?
    
    init(withData data: [String: Any])
    {
        if let id = data["artistId"] as? Int64,
            let name = data["artistName"] as? String
        {
            self.id = id
            self.name = name
        }
    }
    
    class func parseArtists(fromData data: [String: Any]) -> [Artist]
    {
        var items = [Artist]()
        
        if let artists = data["results"] as? [[String: Any]]
        {
            for artistData in artists
            {
                items.append(Artist(withData: artistData))
            }
        }
        
        return items
    }
}
