//
//  HTTPRequest.swift
//  AlamofireDemo
//
//  Created by Mark Maged on 2/17/18.
//  Copyright © 2018 Mark Maged. All rights reserved.
//

import Foundation
import Alamofire

let baseURL = "https://itunes.apple.com/search"

struct HTTPRequest
{
    static func searchArtists(withText text: String,
                              successBlock: @escaping ([Artist]) -> Void,
                              failureBlock: @escaping (Error) -> Void)
    {
        let parameters: [String: Any] = ["entity": "musicArtist",
                                         "limit": 25,
                                         "term": text]
        
        Alamofire.request(baseURL,
                          method: .get,
                          parameters: parameters,
                          encoding: URLEncoding.default).validate().responseJSON { (response) in
                            
                            if let error = response.error
                            {
                                failureBlock(error)
                                return
                            }
                            
                            let artists = Artist.parseArtists(fromData: response.result.value as! [String: Any])
                            
                            successBlock(artists)
        }
    }
}
