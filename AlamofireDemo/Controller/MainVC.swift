//
//  MainVC.swift
//  AlamofireDemo
//
//  Created by Mark Maged on 2/17/18.
//  Copyright © 2018 Mark Maged. All rights reserved.
//

import UIKit

class MainVC: UIViewController
{
    // MARK: - iVars
    
    var artists = [Artist]()
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var searchField: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - View life cycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    // MARK: - Private Methods
    
    private func proceedSearchAction(withText text: String)
    {
        HTTPRequest.searchArtists(withText: text,
                                  successBlock: { (artists) in
            
                                    self.artists = artists
                                    self.tableView.reloadData()
        }) { (error) in
            print(error)
        }
    }
}

// MARK: - UITableViewDataSource

extension MainVC: UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.artists.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        cell.textLabel?.text = self.artists[indexPath.row].name
        
        return cell
    }
}

// MARK: - UIScrollViewDelegate

extension MainVC: UIScrollViewDelegate
{
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView)
    {
        self.view.endEditing(true)
    }
}

// MARK: - UISearchBarDelegate

extension MainVC: UISearchBarDelegate
{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        self.proceedSearchAction(withText: searchText)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        self.searchField.resignFirstResponder()
        
        if let text = self.searchField.text, !text.isEmpty
        {
            self.proceedSearchAction(withText: text)
        }
    }
}
